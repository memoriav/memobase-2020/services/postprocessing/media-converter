FROM sbtscala/scala-sbt:eclipse-temurin-jammy-21.0.2_13_1.10.4_3.5.2 AS build
ADD . .
RUN sbt assembly && \
mkdir /dist && \
mv target/scala-3.5.2/app.jar /dist

FROM eclipse-temurin:21-jre-jammy
RUN apt-get update && \
apt-get install -y ffmpeg imagemagick && \
apt-get autoremove -y && \
apt-get clean
COPY --from=build /dist/app.jar /app/app.jar
CMD java -jar /app/app.jar

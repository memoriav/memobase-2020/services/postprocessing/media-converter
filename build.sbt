import Dependencies.*

ThisBuild / scalaVersion := "3.5.2"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memobase"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Media Converter",
    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case "log4j.properties"                           => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case "log4j2.xml"                                 => MergeStrategy.first
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },
    assembly / mainClass := Some("ch.memobase.App"),
    git.useGitDescribe := true,
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      //fedoraClient exclude("org.slf4j", "slf4j-simple"),
      kafkaClients,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      memobaseServiceUtils excludeAll (
        ExclusionRule(organization = "org.slf4j", name = "slf4j-api"),
        ExclusionRule(organization = "org.slf4j", name = "slf4j-log4j12"),
        ExclusionRule(organization = "org.slf4j", name = "jcl-over-slf4j"),
      ),
      uPickle,
      scalatic,
      scalaMock % Test,
      scalaTest % Test
    )
  )

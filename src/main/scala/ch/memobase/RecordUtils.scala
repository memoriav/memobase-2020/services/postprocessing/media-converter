/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.*
import ujson.{Str, Value}

import scala.collection.mutable.ArrayBuffer
import scala.util.{Success, Try}

trait RecordUtils {
  protected def getJsonldGraph(msg: String): ArrayBuffer[Value] = {
    ujson.read(msg).obj("@graph").arr
  }

  protected def isDigitalObject(obj: ujson.Obj): Boolean = {
    hasKeyValue(obj, "type") {
      MemobaseResource(_) == DigitalObject
    }
  }

  protected def isPreviewImage(obj: ujson.Obj): Boolean = {
    hasKeyValue(obj, "type") {
      MemobaseResource(_) == VideoThumbnail
    }
  }

  protected def isProcessableMimeType(obj: ujson.Obj): Boolean = {
    hasKeyValue(obj, "hasMimeType") {
      value => Conversions.getMediaFileType(value).isDefined
    }
  }

  protected def isLocalRecord(obj: ujson.Obj, sftpPrefix: String = "sftp:/"): Boolean = {
    hasKeyValue(obj, "locator") {
      _.startsWith(sftpPrefix)
    }
  }

  private def hasKeyValue(obj: ujson.Obj, key: String)(valueFun: String => Boolean): Boolean = {
    Try(obj.value(key)) match {
      case Success(id: Str) => valueFun(id.value)
      case _ => false
    }
  }
}

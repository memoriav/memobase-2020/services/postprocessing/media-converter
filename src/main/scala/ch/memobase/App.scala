/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.*
import ch.memobase.settings.SettingsLoader
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import scala.jdk.CollectionConverters.*
import scala.util.{Failure, Success, Try}

object App extends scala.App with Logging with RecordUtils {
  val settings = new SettingsLoader(
    List(
      "audioSnippetDuration",
      "mediaFolderRootPath",
      "thumbnailFolderPath",
      "thumbnailWidth",
      "thumbnailHeight",
      "distributorUrl",
      "connectionMaxRetries",
      "connectionRetryAfterMs"
    ).asJava,
    "app.yml",
    true,
    false,
    true,
    false
  )
  settings.getKafkaConsumerSettings.setProperty(
    ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,
    "false"
  )
  settings.getKafkaConsumerSettings.setProperty(
    ConsumerConfig.MAX_POLL_RECORDS_CONFIG,
    "10"
  )
  settings.getKafkaConsumerSettings.setProperty(
    ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG,
    "60000"
  )
  settings.getKafkaConsumerSettings.setProperty(
    ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG,
    "600000"
  )

  val consumer =
    new KafkaConsumer[String, String](settings.getKafkaConsumerSettings)
  val fileHandler = new DisseminationCopyHandler(
    settings.getAppSettings.getProperty("audioSnippetDuration").toInt
  )
  private val recordProcessor =
    new RecordProcessor(fileHandler, settings.getAppSettings)
  private val reporter =
    Reporter(settings.getKafkaProducerSettings, settings.getProcessReportTopic)
  private val consumerPollTimeoutMs = 100

  try {
    logger.debug(s"Subscribing to topic ${settings.getInputTopic}")
    consumer.subscribe(List(settings.getInputTopic).asJava)
    while (true) {
      val records =
        consumer.poll(Duration.ofMillis(consumerPollTimeoutMs)).asScala
      val partitions =
        records.foldLeft(Set[Int]())((agg, x) => agg + x.partition())

      if (records.nonEmpty) {
        logger.info(
          s"Fetching ${records.size} documents from partition${if (partitions.size > 1) { "s" }
          else { "" }} ${partitions.mkString(",")} for processing"
        )
      } else {
        logger.debug(
          s"Fetching 0 documents from partition${if (partitions.size > 1) {
            "s"
          } else { "" }} ${partitions.mkString(",")} for processing"
        )
      }
      records.filter(record => record.value != null).foreach { record =>
        {
          val headers = record.headers()
          val reportingObject = recordProcessor
            .process(record)
            .foldLeft(ReportingObject(record.key))((reportingObject, outcome) =>
              outcome match {
                case ProcessSuccess(id, resource, msg) =>
                  logger.info(s"$id: $msg")
                  ReportingObject.addResourceOutcome(
                    reportingObject,
                    (resource, msg),
                    ProcessingSuccess
                  )
                case ProcessIgnore(id, resource, msg) =>
                  logger.info(s"$id: $msg")
                  ReportingObject.addResourceOutcome(
                    reportingObject,
                    (resource, msg),
                    ProcessingIgnore
                  )
                case ProcessWarning(id, resource, msg) =>
                  logger.warn(s"$id: $msg")
                  ReportingObject.addResourceOutcome(
                    reportingObject,
                    (resource, msg),
                    ProcessingWarning
                  )
                case ProcessFatal(id, resource, msg, ex) =>
                  logger.error(s"$id: $msg: ${ex.getMessage}")
                  logger.debug(ex.getStackTrace.mkString("\n"))
                  ReportingObject.addResourceOutcome(
                    reportingObject,
                    (resource, s"$msg: ${ex.getMessage}"),
                    ProcessingFatal
                  )
              }
            )
          reporter.send(reportingObject, headers)
        }
      }
      if (records.nonEmpty) {
        Try(consumer.commitSync(Duration.ofMillis(30000))) match {
          case Success(_) =>
            logger.info(s"${records.size} documents processed and committed")
          case Failure(ex) =>
            logger.error(
              s"${records.size} documents successfully processed, but commit to Kafka failed: ${ex.getMessage}"
            )
        }
      }
    }
  } catch {
    case e: Exception =>
      logger.error(e)
      sys.exit(1)
  } finally {
    logger.info("Shutting down application")
    consumer.close()
    reporter.close()
  }
}

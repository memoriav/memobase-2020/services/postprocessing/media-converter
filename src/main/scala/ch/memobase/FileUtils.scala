/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.MimeType

trait FileUtils {

  import models.Conversions.*

  val rootPath: String
  val cachedImagePath: String

  private val normalize: String => String =
    path => if (path.endsWith("/")) path.substring(0, path.length - 1) else path

  val videoFilePath: (String, MimeType) => String =
    (id, mimeType) => s"${normalize(rootPath)}/$id.${getFileTypeExtension(mimeType).get}"

  val videoPosterPath: (String, MimeType) => String =
    (id, mimeType) => s"${normalize(rootPath)}/${id.replace("/derived", "")}-poster.${getFileTypeExtension(mimeType).get}"

  val audioFilePath: String => String =
    id => s"${normalize(rootPath)}/$id.mp4"

  val audioSnippetPath: String => String =
    id => s"${normalize(rootPath)}/$id-intro.mp3"

  val imageFileRootPath: (String, MimeType) => String =
    (id, mimeType) => imageFilePath(rootPath, id, mimeType, x => x)

  val imageThumbnailFilePath: (String, MimeType) => String =
    (id, mimeType) => imageFilePath(cachedImagePath, id, mimeType, x => x)

  val posterImageThumbnailFilePath: (String, MimeType) => String =
    (id, mimeType) => imageFilePath(cachedImagePath, id, mimeType, _.replace("/derived", "-poster"))

  private val imageFilePath: (String, String, MimeType, String => String) => String =
    (path, id, mimeType, idTransform) => s"${normalize(path)}/${idTransform(id)}.${getFileTypeExtension(mimeType).get}"

}

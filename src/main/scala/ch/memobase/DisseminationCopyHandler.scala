/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{Conversions, MimeType}
import org.apache.logging.log4j.scala.Logging

import java.io.{ByteArrayOutputStream, FileOutputStream, IOException}
import java.nio.file.{Files, Path, Paths}
import scala.util.{Failure, Success, Try}

/**
 * Manages dissemination copies of media files
 */
class DisseminationCopyHandler(audioSnippetDuration: Int) extends Logging {

  private def writeData(data: ByteArrayOutputStream, destFile: Path): Try[Path] = {
    val inSize = data.size
    Try(new FileOutputStream(destFile.toFile)) match {
      case Success(fos) =>
        try {
          data.writeTo(fos)
          Success(destFile)
        } catch {
          case e: IOException => Failure(e)
        } finally {
          val outSize = fos.getChannel.size
          logger.debug(s"$inSize bytes data read, $outSize bytes written for $destFile")
          fos.close()
          data.close()
        }
      case Failure(ex) => Failure(ex)
    }
  }

  /**
   * Removes existing file
   *
   * @param destFile Path to file
   * @return true if removed, false otherwise
   */
  private def removeExistingFile(destFile: Path): Boolean = {
    if (destFile.toFile.exists()) {
      destFile.toFile.delete()
      true
    } else {
      false
    }
  }

  /**
   * Creates dissemination copy of audio file
   *
   * @param data           binary data as [[java.io.ByteArrayOutputStream]] instance
   * @param destFile       Path to dissemination copy
   * @param sourceFileType File type of input data
   * @param isSnippet      Process data as snippet
   * @return true if both files were overwritten, false otherwise
   */
  def createAudioCopy(data: ByteArrayOutputStream, destFile: String, sourceFileType: MimeType, isSnippet: Boolean = false): Try[Boolean] = Try {
    val tempFilePath = Files.createTempFile("media-", "." + Conversions.getFileTypeExtension(sourceFileType).get)
    writeData(data, tempFilePath)
    val copyRemoved = removeExistingFile(Paths.get(destFile))
    if (isSnippet) {
      MediaTransformations.createAudioSnippet(tempFilePath.toString, destFile, audioSnippetDuration)
    } else {
      MediaTransformations.audioToMp4(tempFilePath.toString, destFile).get
    }
    Files.delete(tempFilePath)
    copyRemoved
  }

  /**
   * Creates dissemination copy of image file
   *
   * @param data           binary data as [[java.io.ByteArrayOutputStream]] instance
   * @param destFile       Path to dissemination copy
   * @param sourceFileType File type of input data
   * @return true if copy was overwritten, false otherwise
   */
  def createImageCopy(data: ByteArrayOutputStream, destFile: String, sourceFileType: MimeType): Try[Boolean] = Try {
    val destFileAsPath = Paths.get(destFile)
    val fileRemoved = removeExistingFile(destFileAsPath)
    writeData(data, destFileAsPath)
    fileRemoved
  }

  /**
   * Creates a thumbnail representation of the image
   *
   * @param origFile Path to the dissemination copy of the image file
   * @param destFile Path to the thumbnail representation of the image
   * @param width    Optional width of thumbnail
   * @param height   Optional height of thumbnail
   * @return true if thumbnail was overwritten, false otherwise
   */
  def createImageThumbnail(origFile: String, destFile: String, width: Option[Int], height: Option[Int]): Try[Boolean] = Try {
    val destFileAsPath = Paths.get(destFile)
    val fileRemoved = removeExistingFile(destFileAsPath)
    MediaTransformations.createImageThumbnail(origFile, destFile, width, height).get
    fileRemoved
  }

  /**
   * Create dissemination copy of video file
   *
   * @param data           binary data as [[java.io.ByteArrayOutputStream]] instance
   * @param destFile       Path to dissemination copy
   * @param sourceFileType File type of input data
   * @return true if copy was overwritten, false otherwise
   */
  def createVideoCopy(data: ByteArrayOutputStream, destFile: String, sourceFileType: MimeType): Try[Boolean] = Try {
    val destFileAsPath = Paths.get(destFile)
    val copyRemoved = removeExistingFile(destFileAsPath)
    writeData(data, destFileAsPath)
    copyRemoved
  }
}

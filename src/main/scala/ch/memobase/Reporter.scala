/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.header.Headers
import org.apache.logging.log4j.scala.Logging

import java.util.Properties
import java.util.concurrent.Future as JavaFuture


/**
 * Wrapper class for sending reports via Kafka topic
 *
 * @param props          Properties for wrapped KafkaProducer
 * @param reportingTopic Name of reporting topic
 */
class Reporter(props: Properties, reportingTopic: String) extends Logging {

  private val producer = new KafkaProducer[String, String](props)

  //noinspection ScalaStyle
  def send(report: models.ReportingObject, headers: Headers): JavaFuture[RecordMetadata] = {
    val producerRecord = new ProducerRecord[String, String](
      reportingTopic,
      null,
      report.id,
      report.toString,
      headers
    )
    producer.send(producerRecord)
  }

  def close(): Unit = producer.close()

}

object Reporter {
  def apply(props: Properties, reportingTopic: String): Reporter = new Reporter(props, reportingTopic)
}

/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import org.apache.logging.log4j.scala.Logging

import java.io.IOException
import scala.util.Try

/**
 * Contains functions used to transform specific media files
 */
object MediaTransformations extends Logging {

  import sys.process.*

  private def executeCommand(command: String): Try[Int] = Try {
    logger.debug(s"Execute: $command")
    val stderr = new StringBuilder()
    val errorCode = command ! ProcessLogger(logger.debug(_), stderr append _)
    if (errorCode > 0) {
      val errorMsg = s"application ${command.split(' ')(0)} exited with code $errorCode: ${stderr.toString()}"
      logger.warn(errorMsg)
      throw new IOException(errorMsg)
    } else {
      logger.debug("Command execution successful")
      errorCode
    }
  }

  /**
   * Repacks audio files in a MP4 container and adds a moov atom at the beginning of the file
   *
   * @param sourceFilePath Path to the source file
   * @param destFile       Path to the final file
   * @return
   */
  def audioToMp4(sourceFilePath: String, destFile: String): Try[String] = {
    val externalCommand = s"ffmpeg -i $sourceFilePath -acodec copy -strict -1 -loglevel warning -hide_banner -y -movflags faststart $destFile"
    Try {
      executeCommand(externalCommand).get
      destFile
    }
  }

  /**
   * Creates an audio snippet used as a base for producing sonograms
   *
   * @param sourceFilePath Path to the source file
   * @param destFile       Path to the final file
   * @param duration       Duration of snippet (counts from beginning of track)
   * @return
   */
  def createAudioSnippet(sourceFilePath: String, destFile: String, duration: Int): Try[String] = {
    val minutes = (duration / 60) % 60
    val hours = (duration / 60 / 60) % 24
    val time = "%02d:%02d:%02d".format(hours, minutes, duration % 60)
    val copyStream = sourceFilePath.endsWith(".mp3")
    val externalCommand = s"ffmpeg -i $sourceFilePath ${if (copyStream) "-acodec copy" else ""} -strict -1 -loglevel warning -hide_banner -y -t $time $destFile"
    Try {
      executeCommand(externalCommand)
      destFile
    }
  }

  /**
   * Creates an image thumbnail
   *
   * @param sourceFilePath Path to the source file
   * @param destFile       Path to the final file
   * @param width          Thumbnail's width. If None, width is set relative to height
   * @param height         Thumbnail's height If None, height is set relative to width
   * @return
   */
  def createImageThumbnail(sourceFilePath: String, destFile: String, width: Option[Int], height: Option[Int]): Try[String] = {
    val externalCommand =
      s"""convert
         |-filter Triangle
         |-define filter:support=2
         |-thumbnail ${width.getOrElse("")}x${height.getOrElse("")}
         |-auto-orient
         |-unsharp 0.25x0.08+8.3+0.045
         |-dither None
         |-posterize 136
         |-quality 82
         |-define jpeg:fancy-upsampling=off
         |-define png:compression-filter=5
         |-define png:compression-level=9
         |-define png:compression-strategy=1
         |-define png:exclude-chunk=all
         |-interlace none
         |$sourceFilePath
         |$destFile""".stripMargin.replaceAll("\n", " ")
    Try {
      executeCommand(externalCommand)
      destFile
    }
  }
}

/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

/**
 * Represents a media file manageable by media conversion tools
 */
sealed trait MimeType

/**
 * Represents any manageable audio file
 */
sealed trait AudioFile extends MimeType

/**
 * Represents any manageable image file
 */
sealed trait ImageFile extends MimeType

/**
 * Represents any manageable video file
 */
sealed trait VideoFile extends MimeType

/**
 * Represents an unknown file type
 */
case object UnknownFileType extends MimeType

/**
 * Represents a MP3 file
 */
case object Mp3File extends AudioFile

/**
 * Represents an OGA file
 */
case object OgaFile extends AudioFile

/**
 * Represents a JPG file
 */
case object JpegFile extends ImageFile

/**
 * Represents a PNG file
 */
case object PngFile extends ImageFile

/**
 * Represents a MPEG4 video file
 */
case object VideoMpeg4File extends VideoFile

/**
 * Helps with conversions between different representations of file types
 */
object Conversions {
  private val fileTypeTuples: List[(MimeType, List[String], String)] = List(
    (Mp3File, List("audio/mpeg", "audio/mp3"), "mp3"),
    (OgaFile, List("audio/ogg"), "oga"),
    (JpegFile, List("image/jpeg"), "jpg"),
    (PngFile, List("image/png"), "png"),
    (VideoMpeg4File, List("video/mp4"), "mp4")
    // TODO: Other filetypes...
  )

  lazy val getMediaFileType: String => Option[MimeType] = (mimeType: String) =>
    fileTypeTuples.collectFirst {
      case (ft, mt, _) if mt.contains(mimeType) => ft
    }

  lazy val getFileTypeExtension: MimeType => Option[String] = (mediaFileType: MimeType) =>
    fileTypeTuples.collectFirst {
      case (ft, _, ex) if ft == mediaFileType => ex
    }
}

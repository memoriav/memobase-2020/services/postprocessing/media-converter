/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import java.text.SimpleDateFormat
import java.util.Calendar


sealed trait ProcessingStatus {
  val value: String
}

case object ProcessingSuccess extends ProcessingStatus {
  override val value: String = "SUCCESS"
}

case object ProcessingWarning extends ProcessingStatus {
  override val value: String = "WARNING"
}

case object ProcessingIgnore extends ProcessingStatus {
  override val value: String = "IGNORE"
}

case object ProcessingFatal extends ProcessingStatus {
  override val value: String = "FATAL"
}


case class ReportingObject(id: String,
                           digitalObject: Option[(ProcessingStatus, String)] = None,
                           videoThumbnail: Option[(ProcessingStatus, String)] = None,
                           audioSnippet: Option[(ProcessingStatus, String)] = None,
                           imageThumbnail: Option[(ProcessingStatus, String)] = None) {

  import ReportingObject.*

  override def toString: String =
    ujson.write(
      ujson.Obj(
        ("step", "09.01-media-converter"),
        ("stepVersion", getClass.getPackage.getImplementationVersion),
        ("timestamp", createTimestamp),
        ("id", id),
        ("status", mergeStatus(digitalObject, videoThumbnail, audioSnippet, imageThumbnail)),
        ("message", mergeMessages(digitalObject, videoThumbnail, audioSnippet, imageThumbnail))
      )
    )
}

object ReportingObject {
  private val dateFormatter = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSS")

  private def createTimestamp: String = dateFormatter.format(Calendar.getInstance().getTime)

  private def mergeStatus(digitalObject: Option[(ProcessingStatus, String)],
                  videoThumbnail: Option[(ProcessingStatus, String)],
                  audioSnippet: Option[(ProcessingStatus, String)],
                  imageThumbnail: Option[(ProcessingStatus, String)]): String = {
    val processStatus = List(digitalObject, videoThumbnail, audioSnippet, imageThumbnail)
      .flatMap(obj => obj.flatMap(o => Some(o._1)))
    if (processStatus.contains(ProcessingFatal)) {
      ProcessingFatal.value
    } else if (processStatus.contains(ProcessingWarning)) {
      ProcessingWarning.value
    } else if (processStatus.contains(ProcessingSuccess)) {
      ProcessingSuccess.value
    } else {
      ProcessingIgnore.value
    }
  }

  private def mergeMessages(digitalObject: Option[(ProcessingStatus, String)],
                    videoThumbnail: Option[(ProcessingStatus, String)],
                    audioSnippet: Option[(ProcessingStatus, String)],
                    imageThumbnail: Option[(ProcessingStatus, String)]): String =
    s"""DIGITAL OBJECT: ${digitalObject.flatMap(dO => Some(dO._2)).getOrElse("not available")}
       | -- VIDEO THUMBNAIL: ${videoThumbnail.flatMap(p => Some(p._2)).getOrElse("not available")}
       | -- AUDIO SNIPPET:${audioSnippet.flatMap(aS => Some(aS._2)).getOrElse("not available")}
       | -- IMAGE THUMBNAIL: ${imageThumbnail.flatMap(iT => Some(iT._2)).getOrElse("not available")}""".stripMargin

  def addResourceOutcome(report: ReportingObject, resource: (MemobaseResource, String), status: ProcessingStatus): ReportingObject =
    resource._1 match {
      case DigitalObject => report.copy(digitalObject = Some(status, resource._2))
      case VideoThumbnail => report.copy(videoThumbnail = Some(status, resource._2))
      case AudioSnippet => report.copy(audioSnippet = Some(status, resource._2))
      case ImageThumbnail => report.copy(imageThumbnail = Some(status, resource._2))
      case _ => report
    }
}

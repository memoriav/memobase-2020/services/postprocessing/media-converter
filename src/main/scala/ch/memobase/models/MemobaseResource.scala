/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

/**
 * Represents an instantiation type as defined in the Memobase internal data model
 */
sealed trait MemobaseResource

/**
 * Represents a digital object
 */
case object DigitalObject extends MemobaseResource

/**
 * Represents a physical object
 */
case object PhysicalObject extends MemobaseResource

/**
 * Represents a video thumbnail
 */
case object VideoThumbnail extends MemobaseResource

/**
 * Represents an image thumbnail
 */
case object ImageThumbnail extends MemobaseResource

/**
 * Represents a record
 */
case object Record extends MemobaseResource

/**
 * Represents a audio snippet. This instantiation doesn't
 * exist in the data model, however it is used internally
 * by the service to differentiate between several derived
 * types.
 */
case object AudioSnippet extends MemobaseResource

/**
 * Unknown instantiation
 */
case object UnknownResource extends MemobaseResource


/**
 * Helper functions for Instantiation type
 */
object MemobaseResource {
  def apply(i: String): MemobaseResource = i match {
    case "digitalObject" => DigitalObject
    case "thumbnail" => VideoThumbnail
    case "physicalObject" => PhysicalObject
    case _ => UnknownResource
  }
}
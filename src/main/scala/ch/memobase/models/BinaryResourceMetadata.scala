/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

import ch.memobase.RecordUtils
import ujson.Value

import scala.collection.mutable.ArrayBuffer
import scala.util.Try

/** Essential information on a binary file residing in Fedora
  *
  * @param id
  *   Identifier of the binary file
  * @param filePath
  *   File path (URL) to resource
  * @param mimeType
  *   MIME type
  * @param resource
  *   Type of instantiation
  */
case class BinaryResourceMetadata(
    id: String,
    filePath: String,
    mimeType: MimeType,
    resource: MemobaseResource
) {}

object BinaryResourceMetadata extends RecordUtils {

  /** Builds a `BinaryResourceMetadata` object from a JSON-LD object pulled from
    * Kafka topic
    *
    * @param msg
    *   Pulled Kafka message
    * @param distributorHost
    *   Host and port of media distributor service
    * @return
    */
  def build(
      msg: String,
      distributorHost: String
  ): List[Try[BinaryResourceMetadata]] = {
    val jsonldGraph = getJsonldGraph(msg)
    extractBinaryResourceMetadata(jsonldGraph, distributorHost)
  }

  private def buildDistributorUrl(
      fileName: String,
      distributorHost: String,
      resourceType: MemobaseResource
  ): String = {
    resourceType match {
      case DigitalObject =>
        s"${distributorHost.stripSuffix("/")}/media/$fileName"
      case VideoThumbnail =>
        s"${distributorHost.stripSuffix("/")}/thumbnail/$fileName"
      case _ =>
        ""
    }
  }

  //noinspection ScalaStyle
  private def extractBinaryResourceMetadata(
      jsonldGraph: ArrayBuffer[Value],
      distributorHost: String
  ): List[Try[BinaryResourceMetadata]] =
    jsonldGraph.value
      .withFilter { v =>
        isDigitalObject(v.obj) || isPreviewImage(v.obj)
      }
      .map { v =>
        {
          val resourceId = v.obj.getOrElse("@id", Value("<unknown id>")).str
          Try(
            v.obj match {
              case v if isLocalRecord(v) && isProcessableMimeType(v) =>
                val instantiation = MemobaseResource(v("type").str)
                val binaryObjectId =
                  v("@id").str.substring(s"mbdo:".length)
                val locator = {
                  val basename = {
                    val filename = v("locator").str.split("/").last
                    filename
                      .splitAt(filename.lastIndexOf("."))
                      ._1
                      .replaceAll(" ", "_")
                  }
                  val recordSetId = binaryObjectId.split('-').take(2).mkString("-")
                  s"$recordSetId-$basename"
                }
                BinaryResourceMetadata(
                  binaryObjectId,
                  buildDistributorUrl(locator, distributorHost, instantiation),
                  Conversions.getMediaFileType(v("hasMimeType").str).get,
                  instantiation
                )
              case v if isLocalRecord(v) =>
                val resource = MemobaseResource(v("type").str)
                throw new UnmanageableMediaFileType(
                  s"Media file type for $resourceId unknown",
                  resource
                )
              case v =>
                val resource = MemobaseResource(v("type").str)
                throw new NoLocalBinary(resourceId, resource)
            }
          )
        }
      }
      .toList
}

/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase.models

/**
 * If the file type is not meant to be handled by the media transformation tools
 *
 * @param msg dedicated error message
 */
//noinspection ScalaFileName
class UnmanageableMediaFileType(msg: String, val resource: MemobaseResource) extends Exception(msg)

class NoDigitalObject extends Exception("No digital object found")

class NoLocalBinary(id: String, val resource: MemobaseResource) extends Exception(s"No reference to local binary in $id found")


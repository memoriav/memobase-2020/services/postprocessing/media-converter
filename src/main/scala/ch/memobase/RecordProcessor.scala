/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.*
import org.apache.kafka.clients.consumer.ConsumerRecord

import java.io.{ByteArrayOutputStream, InputStream}
import java.net.{ConnectException, URI}
import java.util.Properties
import scala.util.{Failure, Success, Try}

sealed trait ProcessOutcome

case class ProcessSuccess(id: String, resource: MemobaseResource, msg: String)
    extends ProcessOutcome

case class ProcessFatal(
    id: String,
    resource: MemobaseResource,
    msg: String,
    ex: Throwable
) extends ProcessOutcome

case class ProcessWarning(id: String, resource: MemobaseResource, msg: String)
    extends ProcessOutcome

case class ProcessIgnore(id: String, resource: MemobaseResource, msg: String)
    extends ProcessOutcome

class RecordProcessor(
    fileHandler: DisseminationCopyHandler,
    appSettings: Properties
) extends FileUtils {

  val rootPath: String = appSettings.getProperty("mediaFolderRootPath")
  val cachedImagePath: String = appSettings.getProperty("thumbnailFolderPath")
  val distributorUrl: String = appSettings.getProperty("distributorUrl")
  private val maxRetries: Int =
    appSettings.getProperty("connectionMaxRetries").toInt
  private val retryAfter: Int =
    appSettings.getProperty("connectionRetryAfterMs").toInt

  def process(record: ConsumerRecord[String, String]): List[ProcessOutcome] = {
    BinaryResourceMetadata.build(record.value(), distributorUrl) flatMap {
      case Success(binaryResource) =>
        handleBinaryResource(binaryResource, record.key())
      case Failure(ex) =>
        List(ex match {
          case e: NoLocalBinary =>
            ProcessIgnore(record.key(), e.resource, e.getMessage)
          case e: NoDigitalObject =>
            ProcessIgnore(record.key(), DigitalObject, e.getMessage)
          case e: UnmanageableMediaFileType =>
            ProcessWarning(record.key(), e.resource, e.getMessage)
          case e: Exception =>
            ProcessFatal(record.key(), Record, e.getMessage, e)
        })
    }
  }

  private def handleBinaryResource(
      binaryResource: BinaryResourceMetadata,
      recordKey: String
  ): List[ProcessOutcome] = {
    fetchBinaryResource(binaryResource.filePath) match {
      case Success(tempFilePath) =>
        createResource(
          binaryResource.id,
          binaryResource.mimeType,
          binaryResource.resource,
          tempFilePath
        )
      case Failure(ex) =>
        List(
          ProcessFatal(
            recordKey,
            binaryResource.resource,
            s"Failed to retrieve binary from Media File Distributor with URL ${binaryResource.filePath}",
            ex
          )
        )
    }
  }

  private def createOutcome(
      res: Try[Boolean],
      id: String,
      resource: MemobaseResource,
      destFile: String
  ): List[ProcessOutcome] = List(res match {
    case Success(true) =>
      ProcessSuccess(id, resource, s"Updating of file $destFile successful")
    case Success(false) =>
      ProcessSuccess(id, resource, s"Creation of file $destFile successful")
    case Failure(ex) =>
      ProcessFatal(id, resource, s"Creation of file $destFile failed", ex)
  })

  private def createResource(
      id: String,
      mimeType: MimeType,
      resource: MemobaseResource,
      data: ByteArrayOutputStream
  ): List[ProcessOutcome] = mimeType match {
    case mT: AudioFile =>
      List(
        (audioFilePath(id), false, DigitalObject),
        (audioSnippetPath(id), true, AudioSnippet)
      )
        .map(path =>
          (
            fileHandler.createAudioCopy(data, path._1, mT, path._2),
            path._1,
            path._3
          )
        )
        .flatMap(x => createOutcome(x._1, id, x._3, x._2))
    case mT: VideoFile =>
      val destFile = videoFilePath(id, mT)
      val res = fileHandler.createVideoCopy(data, destFile, mT)
      createOutcome(res, id, DigitalObject, destFile)
    case mT: ImageFile if resource == DigitalObject =>
      val destFile = imageFileRootPath(id, mT)
      createImageAndThumbnail(
        id,
        data,
        destFile,
        mT,
        DigitalObject,
        imageThumbnailFilePath
      )
    case mT: ImageFile if resource == VideoThumbnail =>
      val destFile = videoPosterPath(id, mT)
      createImageAndThumbnail(
        id,
        data,
        destFile,
        mT,
        VideoThumbnail,
        posterImageThumbnailFilePath
      )
  }

  private def createImageAndThumbnail(
      id: String,
      data: ByteArrayOutputStream,
      destImageFile: String,
      mimeType: MimeType,
      memobaseResource: MemobaseResource,
      thumbnailFilePathFun: (String, MimeType) => String
  ): List[ProcessOutcome] = {
    val resMediaFile =
      fileHandler.createImageCopy(data, destImageFile, mimeType)
    val outcomeMediaFile =
      createOutcome(resMediaFile, id, memobaseResource, destImageFile)
    val destPreviewFile = thumbnailFilePathFun(id, mimeType)
    val (width, height) = getThumbnailDimensions
    val resThumbnail = fileHandler.createImageThumbnail(
      destImageFile,
      destPreviewFile,
      width,
      height
    )
    val outcomeThumbnail =
      createOutcome(resThumbnail, id, ImageThumbnail, destPreviewFile)
    outcomeMediaFile ++ outcomeThumbnail
  }

  private def getThumbnailDimensions: (Option[Int], Option[Int]) = {
    (
      Try(appSettings.getProperty("thumbnailWidth").toInt).toOption
        .filter(_ >= 1),
      Try(appSettings.getProperty("thumbnailHeight").toInt).toOption
        .filter(_ >= 1)
    )
  }

  /** Fetches a binary resource from remote API and returns, if successful, a
    * [[scala.io.BufferedSource]] instance
    *
    * @param url
    *   The URL to the resource
    * @return
    */
  private def fetchBinaryResource(url: String): Try[ByteArrayOutputStream] =
    Try {
      val baos = new ByteArrayOutputStream()
      val inputStream = getStream(url)
      Iterator
        .continually(inputStream.read)
        .takeWhile(_ != -1)
        .foreach(baos.write)
      baos
    }

  private def getStream(url: String, retries: Int = 0): InputStream = (Try {
    val u = URI.create(url).toURL
    u.openStream
  } recover {
    case _: ConnectException if retries <= maxRetries =>
      Thread.sleep(retryAfter)
      getStream(url, retries + 1)
  }).get
}

/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.*
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite

import java.io.ByteArrayOutputStream
import scala.io.Source

class RecordProcessorTest extends AnyFunSuite with MockFactory {

  def createIncomingMessage(mimeType: MimeType, hasBinaryResource: Boolean = true): (String, ByteArrayOutputStream) = {
    if (hasBinaryResource) {
      val mT = mimeType match {
        case JpegFile => "image/jpeg"
        case Mp3File => "audio/mpeg"
        case VideoMpeg4File => "video/mpeg"
      }
      replaceTokensInIncomingMessage(mT)
    } else {
      val is = Source.fromFile("src/test/resources/incoming_message_without_binary.json")
      val incomingMessage = is.mkString
      val baos = copyIncomingMessage(incomingMessage)
      is.close
      (incomingMessage, baos)
    }
  }

  private def replaceTokensInIncomingMessage(mimeType: String, locator: String = "https://memobase.ch/digital/BAZ-MEI_77466-1/binary") = {
    val is = Source.fromFile("src/test/resources/incoming_message_with_binary.json")
    val incomingMessage = is.mkString
      .replaceAll(raw"\{\{mimeType\}\}", mimeType)
      .replaceAll(raw"\{\{locator\}\}", locator)
    val baos = copyIncomingMessage(incomingMessage)
    is.close
    (incomingMessage, baos)
  }

  private def copyIncomingMessage(incomingMessage: String): ByteArrayOutputStream = {
    val incomingMessageAsBytes = incomingMessage.getBytes()
    val baos = new ByteArrayOutputStream()
    baos.write(incomingMessageAsBytes, 0, incomingMessageAsBytes.length)
    baos
  }
}

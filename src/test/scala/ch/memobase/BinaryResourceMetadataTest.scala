/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.models.{BinaryResourceMetadata, NoLocalBinary, PngFile, UnmanageableMediaFileType}
import org.scalatest.funsuite.AnyFunSuite

import scala.io.Source

class BinaryResourceMetadataTest extends AnyFunSuite {

  private def loadMessageWithBinaryResource(mimeType: String, locator: String): String = {
    val file = Source.fromFile("src/test/resources/incoming_message_with_binary.json")
    val result = file.mkString
      .replaceAll(raw"\{\{mimeType}}", mimeType)
      .replaceAll(raw"\{\{locator}}", locator)
    file.close()
    result
  }

  test("the value of the id field of a KafkaMessage should match the id of the parsed object") {
    val km = BinaryResourceMetadata.build(loadMessageWithBinaryResource("image/png", "sftp:/swissbib_index/baz-001/media/BAZ-MEI_77466-1.jpg"), "mb-wf2")
    assert(km.head.isSuccess)
  }

  test("a reference to a non-local binary should throw a NoLocalBinary exception") {
    assertThrows[NoLocalBinary] {
      BinaryResourceMetadata.build(loadMessageWithBinaryResource("image/jpeg", "https://example.com"), "mb-wf2")
        .head
        .get
    }
  }

  test("a unmanageable mime type should throw a UnmanageableMediaFileType exception") {
    assertThrows[UnmanageableMediaFileType] {
      BinaryResourceMetadata.build(loadMessageWithBinaryResource("application/pdf",
        "sftp:/swissbib_index/mb_sftp/baz-001/thumbnails/BAZ-MEI_77466-1.jpg"), "mb-wf2")
        .head
        .get
    }
  }

  test("a thumbnail parsing should return the correct mimetype") {
    val inData = Source.fromFile("src/test/resources/incoming_message_with_thumbnail.json")
    val brm = BinaryResourceMetadata.build(inData.mkString, "innerUrl")
    assert(brm(1).get.mimeType == PngFile)
  }
}

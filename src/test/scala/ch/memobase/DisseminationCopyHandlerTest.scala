/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import ch.memobase.TestUtilities.*
import ch.memobase.models.{JpegFile, MimeType, Mp3File, VideoMpeg4File}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.{Assertion, BeforeAndAfter}
import scala.language.reflectiveCalls

import java.io.{ByteArrayOutputStream, File, FileInputStream}
import java.nio.file.{Files, Paths}
import scala.util.Try

class DisseminationCopyHandlerTest extends AnyFunSuite with BeforeAndAfter {
  
  val resPath = "src/test/resources"
  val fileHandler = new DisseminationCopyHandler(30)

  private def deleteFiles(files: String*): Unit = {
    for (file <- files) {
      val p = new File(file).toPath
      Files.deleteIfExists(p)
    }
  }

  private def testCopy(
      pathToTmpDir: String,
      sourceFileName: String,
      destFileName: String,
      fileType: MimeType,
      copyFun: (ByteArrayOutputStream, String, MimeType) => Try[Boolean]
  ): Assertion = {
    val data = createByteArrayOutputStream(pathToTmpDir, sourceFileName)
    val destFile = Paths.get(pathToTmpDir, destFileName)
    destFile.toFile.deleteOnExit()
    copyFun(data, destFile.toString, fileType)
    assert(destFile.toFile.exists())
  }

  private def createByteArrayOutputStream(
      pathToTmpDir: String,
      sourceFileName: String
  ): ByteArrayOutputStream = {
    val file = Paths.get(pathToTmpDir, sourceFileName).toFile
    val data = new ByteArrayOutputStream(file.length().toInt)
    val buffer = new Array[Byte](1024)
    val in = new FileInputStream(file)
    var len = in.read(buffer)
    while (len != -1) {
      data.write(buffer, 0, len)
      len = in.read(buffer)
    }
    data
  }

  private def testAudioCopy(
      pathToTmpDir: String,
      sourceFileName: String,
      destFileName: String,
      fileType: MimeType,
      isSnippet: Boolean,
      copyFun: (
          ByteArrayOutputStream,
          String,
          MimeType,
          Boolean
      ) => Try[Boolean]
  ): Assertion = {
    val file = Paths.get(pathToTmpDir, sourceFileName).toFile
    val data = new ByteArrayOutputStream(file.length().toInt)
    val buffer = new Array[Byte](1024)
    val in = new FileInputStream(file)
    var len = in.read(buffer)
    while (len != -1) {
      data.write(buffer, 0, len)
      len = in.read(buffer)
    }
    val destFile = Paths.get(pathToTmpDir, destFileName)
    destFile.toFile.deleteOnExit()
    copyFun(data, destFile.toString, fileType, isSnippet)
    assert(destFile.toFile.exists())

  }

  /** ATTENTION: Requires that ffmpeg is properly installed!
    */
  test("calling the copyAudio function should create temporary snippet") {
    runWithFFmpeg {
      testAudioCopy(
        resPath,
        "sample.mp3",
        "test-intro.mp3",
        Mp3File,
        isSnippet = true,
        fileHandler.createAudioCopy
      )
      deleteFiles("src/test/resources/test-intro.mp3")
    }
  }

  /** ATTENTION: Requires that ffmpeg is properly installed!
    */
  test("calling the copyAudio function should create temporary file") {
    runWithFFmpeg {
      testAudioCopy(
        resPath,
        "sample.mp3",
        "test.mp4",
        Mp3File,
        isSnippet = false,
        fileHandler.createAudioCopy
      )
      deleteFiles("src/test/resources/test.mp4")
    }
  }

  test("calling the createImageCopy function should create temporary file") {
    testCopy(
      resPath,
      "sample.jpg",
      "test.jpg",
      JpegFile,
      fileHandler.createImageCopy
    )
    deleteFiles("src/test/resources/test.jpg")
  }

  /** ATTENTION: Needs the convert executable on $PATH
    */
  test(
    "calling the createImageThumbnail function should create a temporary thumbnail file"
  ) {
    val testImagePath = Paths.get(resPath, "test.jpg")
    testImagePath.toFile.deleteOnExit()
    val testThumbnailPath = Paths.get(resPath, "test-thumbnail.jpg")
    testThumbnailPath.toFile.deleteOnExit()
    val data = createByteArrayOutputStream(resPath, "sample.jpg")
    fileHandler.createImageCopy(data, testImagePath.toString, JpegFile)
    val res = fileHandler.createImageThumbnail(
      testImagePath.toString,
      testThumbnailPath.toString,
      Some(640),
      None
    )
    assert(res.isSuccess)
    assert(testThumbnailPath.toFile.exists())
  }

  /** ATTENTION: Requires that ffmpeg is properly installed!
    */
  test("calling the createVideoCopy function should create temporary file") {
    runWithFFmpeg {
      testCopy(
        resPath,
        "sample.mp4",
        "test.mp4",
        VideoMpeg4File,
        fileHandler.createVideoCopy
      )
      deleteFiles("src/test/resources/test.mp4")
    }
  }

}

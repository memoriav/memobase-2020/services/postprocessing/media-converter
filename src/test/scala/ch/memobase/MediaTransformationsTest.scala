/*
 * Media Converter
 * Extracts media files from Fedora repository
 * Copyright (C) 2021  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ch.memobase

import java.nio.file.{Files, Paths}

import org.scalatest.funsuite.AnyFunSuite
import TestUtilities.*

class MediaTransformationsTest extends AnyFunSuite {

  test("Sample mp3 should be transformed correctly") {
    runWithFFmpeg {
      val outFile = Files.createTempFile(Paths.get("src/test/resources"), "test-", ".mp4")
      val res = MediaTransformations.audioToMp4("src/test/resources/sample.mp3", outFile.toString)
      outFile.toFile.delete()
      assert(res.get == outFile.toString)
    }
  }

  test("Conversion of nonexistent mp3 should abort with error") {
    runWithFFmpeg {
        val outFile = Files.createTempFile(Paths.get("src/test/resources"), "test-", ".mp4")
        val res = MediaTransformations.audioToMp4("src/test/resources/null.mp3", outFile.toString)
        outFile.toFile.delete()
        assert(res.isFailure)
    }
  }

}
